import datetime
import os
import subprocess

import pandas as pd
from sqlalchemy import create_engine

import mazdap as mz

conn = mz.ObieeConnector()

os.environ["NLS_LANG"] = ".AL32UTF8"

engine = create_engine(
    "oracle+cx_oracle://RPR_STG:rpSgdEv12@x4ebid10.mnao.net:1521/EDWHDEV",
    max_identifier_length=128,
)
## get the necessary tables

#### NATIONAL ####
mazda_nat_df = pd.read_sql(
    """\
SELECT
	CAL_DATE AS VDATE,
	SUM(RAW_VISITS) AS PROJECTED_VISITS
FROM
	WP_CUEBIQ_DAILY_VISITS wcdv
INNER JOIN WP_CUEBIQ_PLACE_ID_MAPPING wcpim ON
	wcdv.PLACE_ID = wcpim.PLACE_ID
GROUP BY
	CAL_DATE
""".strip(),
    engine,
)

honda_nat_df = pd.read_sql(
    """\
SELECT
	CAL_DATE AS VDATE,
	SUM(RAW_VISITS) AS PROJECTED_VISITS
FROM
	WP_CUEBIQ_DAILY_VISITS wcdv
INNER JOIN WP_CUEBIQ_PLACE_ID_COMP_MAPPING wcpicm ON
	wcdv.PLACE_ID = wcpicm.PLACE_ID
WHERE
	GEOSET_NAME = 'HONDA_MAR18_ANALYTICS'
GROUP BY
	CAL_DATE
""".strip(),
    engine,
)

hyundai_nat_df = pd.read_sql(
    """\
SELECT
	CAL_DATE AS VDATE,
	SUM(RAW_VISITS) AS PROJECTED_VISITS
FROM
	WP_CUEBIQ_DAILY_VISITS wcdv
INNER JOIN WP_CUEBIQ_PLACE_ID_COMP_MAPPING wcpicm ON
	wcdv.PLACE_ID = wcpicm.PLACE_ID
WHERE
	GEOSET_NAME = 'CUSTOM_FOR_MAZDAMEETING_HYUNDAI_ANALYTICS'
GROUP BY
	CAL_DATE
""".strip(),
    engine,
)

subaru_nat_df = pd.read_sql(
    """\
SELECT
	CAL_DATE AS VDATE,
	SUM(RAW_VISITS) AS PROJECTED_VISITS
FROM
	WP_CUEBIQ_DAILY_VISITS wcdv
INNER JOIN WP_CUEBIQ_PLACE_ID_COMP_MAPPING wcpicm ON
	wcdv.PLACE_ID = wcpicm.PLACE_ID
WHERE
	GEOSET_NAME = 'SUBARU_MAR18_ANALYTICS'
GROUP BY
	CAL_DATE
""".strip(),
    engine,
)

toyota_nat_df = pd.read_sql(
    """\
SELECT
	CAL_DATE AS VDATE,
	SUM(RAW_VISITS) AS PROJECTED_VISITS
FROM
	WP_CUEBIQ_DAILY_VISITS wcdv
INNER JOIN WP_CUEBIQ_PLACE_ID_COMP_MAPPING wcpicm ON
	wcdv.PLACE_ID = wcpicm.PLACE_ID
WHERE
	GEOSET_NAME = 'TOYOTA_MAR18_ANALYTICS'
GROUP BY
	CAL_DATE
""".strip(),
    engine,
)

volkswagen_nat_df = pd.read_sql(
    """\
SELECT
	CAL_DATE AS VDATE,
	SUM(RAW_VISITS) AS PROJECTED_VISITS
FROM
	WP_CUEBIQ_DAILY_VISITS wcdv
INNER JOIN WP_CUEBIQ_PLACE_ID_COMP_MAPPING wcpicm ON
	wcdv.PLACE_ID = wcpicm.PLACE_ID
WHERE
	GEOSET_NAME = 'VOLKSWAGEN_ANALYTICS'
GROUP BY
	CAL_DATE
""".strip(),
    engine,
)
#### NATIONAL ####

#### DMA ####
mazda_dma_df = pd.read_sql(
"""\
SELECT
	CAL_DATE AS VDATE,
	MDA_CD AS MDA_CODE,
	SUM(RAW_VISITS) AS PROJECTED_VISITS
FROM
	WP_CUEBIQ_DAILY_VISITS wcdv
INNER JOIN WP_CUEBIQ_PLACE_ID_MAPPING wcpim ON
	wcdv.PLACE_ID = wcpim.PLACE_ID
LEFT JOIN C00_DLY_DEALER_INFO_FW cddif ON
	wcpim.DLR_CD = cddif.DLR_CD
GROUP BY (CAL_DATE, MDA_CD)
""".strip(), engine)

honda_dma_df = pd.read_sql(
    """\
SELECT
	CAL_DATE AS VDATE,
	wcdv.PLACE_ID AS PLACE_ID,
	DMA,
	SUM(RAW_VISITS) AS PROJECTED_VISITS
FROM
	WP_CUEBIQ_DAILY_VISITS wcdv
INNER JOIN WP_CUEBIQ_PLACE_ID_COMP_MAPPING wcpicm ON
	wcdv.PLACE_ID = wcpicm.PLACE_ID
WHERE
	GEOSET_NAME = 'HONDA_MAR18_ANALYTICS'
GROUP BY (CAL_DATE, wcdv.PLACE_ID, DMA)
""".strip(), engine)

hyundai_dma_df = pd.read_sql(
    """\
SELECT
	CAL_DATE AS VDATE,
	wcdv.PLACE_ID AS PLACE_ID,
	DMA,
	SUM(RAW_VISITS) AS PROJECTED_VISITS
FROM
	WP_CUEBIQ_DAILY_VISITS wcdv
INNER JOIN WP_CUEBIQ_PLACE_ID_COMP_MAPPING wcpicm ON
	wcdv.PLACE_ID = wcpicm.PLACE_ID
WHERE
	GEOSET_NAME = 'CUSTOM_FOR_MAZDAMEETING_HYUNDAI_ANALYTICS'
GROUP BY (CAL_DATE, wcdv.PLACE_ID, DMA) 
""".strip(), engine)

subaru_dma_df = pd.read_sql(
    """\
SELECT
	CAL_DATE AS VDATE,
	wcdv.PLACE_ID AS PLACE_ID,
	DMA,
	SUM(RAW_VISITS) AS PROJECTED_VISITS
FROM
	WP_CUEBIQ_DAILY_VISITS wcdv
INNER JOIN WP_CUEBIQ_PLACE_ID_COMP_MAPPING wcpicm ON
	wcdv.PLACE_ID = wcpicm.PLACE_ID
WHERE
	GEOSET_NAME = 'SUBARU_MAR18_ANALYTICS'
GROUP BY (CAL_DATE, wcdv.PLACE_ID, DMA)
""".strip(), engine)

toyota_dma_df = pd.read_sql(
    """\
SELECT
	CAL_DATE AS VDATE,
	wcdv.PLACE_ID AS PLACE_ID,
	DMA,
	SUM(RAW_VISITS) AS PROJECTED_VISITS
FROM
	WP_CUEBIQ_DAILY_VISITS wcdv
INNER JOIN WP_CUEBIQ_PLACE_ID_COMP_MAPPING wcpicm ON
	wcdv.PLACE_ID = wcpicm.PLACE_ID
WHERE
	GEOSET_NAME = 'TOYOTA_MAR18_ANALYTICS'
GROUP BY (CAL_DATE, wcdv.PLACE_ID, DMA)    
""".strip(), engine)

volkswagen_dma_df = pd.read_sql(
    """\
SELECT
	CAL_DATE AS VDATE,
	wcdv.PLACE_ID AS PLACE_ID,
	DMA,
	SUM(RAW_VISITS) AS PROJECTED_VISITS
FROM
	WP_CUEBIQ_DAILY_VISITS wcdv
INNER JOIN WP_CUEBIQ_PLACE_ID_COMP_MAPPING wcpicm ON
	wcdv.PLACE_ID = wcpicm.PLACE_ID
WHERE
	GEOSET_NAME = 'VOLKSWAGEN_ANALYTICS'
GROUP BY (CAL_DATE, wcdv.PLACE_ID, DMA)    
""".strip(), engine)
#### DMA ####

#### DLR/MKT ####
dlr_master = conn.get_csv("/shared/Operational Strategy/Governance and Process Innovation/WaiPhyo/Dim_Master/All Dealers")
mkt_master = conn.get_csv("/shared/Operational Strategy/Governance and Process Innovation/WaiPhyo/Dim_Master/All Markets")
#### DLR/MKT ####

#### MAPPING ####
kg_mapping = pd.read_sql("SELECT * FROM KG_DMA_MDA_MAPPING36", engine)
wp_mapping = pd.read_sql("SELECT * FROM WP_DMA_MDA_MAPPING", engine)
#### MAPPING ####

#### DATE ####
d = datetime.date.today()
yesterday = d - datetime.timedelta(days=1)
date_master = pd.DataFrame({"cal_date": pd.date_range(start="2019-01-03", end=yesterday)})
today_str = yesterday.strftime("%Y-%m-%d")
cal_query = f"WHERE cal_date BETWEEN to_date('2019-01-03', 'yyyy-mm-dd') AND to_date('{today_str}', 'yyyy-mm-dd')"
cal_master = pd.read_sql(
    f"SELECT cal_date, sales_yr_mo_id, sales_mo_name_yr, sales_mo_total_days FROM RPR_STG.KHN_CALENDAR_MASTER {cal_query}",
    engine,
)
date_master = date_master.merge(cal_master, how="left", on="cal_date")

date_shift = conn.get_csv(
    "/shared/Operational Strategy/Governance and Process Innovation/Mazdap/date_shift"
)
#### DATE ####

#### WRITE ####
mazda_nat_df.to_csv("data/mazda_nat_raw.csv", index=False)
honda_nat_df.to_csv("data/honda_nat_raw.csv", index=False)
hyundai_nat_df.to_csv("data/hyundai_nat_raw.csv", index=False)
subaru_nat_df.to_csv("data/subaru_nat_raw.csv", index=False)
toyota_nat_df.to_csv("data/toyota_nat_raw.csv", index=False)
volkswagen_nat_df.to_csv("data/volkswagen_nat_raw.csv", index=False)

mazda_dma_df.to_csv("data/mazda_dma_raw.csv", index=False)
honda_dma_df.to_csv("data/honda_dma_raw.csv", index=False)
hyundai_dma_df.to_csv("data/hyundai_dma_raw.csv", index=False)
subaru_dma_df.to_csv("data/subaru_dma_raw.csv", index=False)
toyota_dma_df.to_csv("data/toyota_dma_raw.csv", index=False)
volkswagen_dma_df.to_csv("data/volkswagen_dma_raw.csv", index=False)

dlr_master.to_csv("data/dlr_master.csv", index=False)
mkt_master.to_csv("data/mkt_master.csv", index=False)
kg_mapping.to_csv("data/kg_mapping.csv", index=False)
wp_mapping.to_csv("data/wp_mapping.csv", index=False)
date_master.to_csv("data/date_master.csv", index=False)
date_shift.to_csv("data/date_shift.csv", index=False)
#### WRITE ####

conn.logoff()

#### SYNC ONEDRIVE ####
sync_cmd = "rclone sync -v data sharepoint:wphyo/cuebiq"
with open("logging.txt", "w") as f:
    process = subprocess.Popen(sync_cmd, shell=True, stderr=f, universal_newlines=True)
    while True:
        return_code = process.poll()
        if return_code is not None:
            break
#### SYNC ONEDRIVE ####
